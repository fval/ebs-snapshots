# ebs-snapshots

Automation script for EC2's EBS scheduled backups into AWS snapshots.

This project was based upon [Flynsarmy](http://jayaraj.sosblogs.com/The-first-blog-b1/AWS-Automated-EBS-Snapshot-Script-b1-p3.htm)'s work.

# Prerequisites

-   Python 3
-   [AWS IAM user](https://aws.amazon.com/iam/)
-   [AWS command line tools](https://docs.aws.amazon.com/pt_br/cli/latest/userguide/cli-chap-install.html)

# Instalation

## Step 1 - The AWS IAM user

We need an [IAM](https://aws.amazon.com/iam/) user with permissions only to do what our backup script requires.

If you already have one, jump to next step.

If you don't have one, create it in the IAM section of AWS console. Give it a good name (I named mine `snapshot-backup`) and save the credentials in a safe place (*this is very very important*).

In the user's Inline Policies area give it the following policy:

```json
{
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "ec2:CreateSnapshot",
                "ec2:CreateTags",
                "ec2:DeleteSnapshot",
                "ec2:DescribeSnapshots",
                "ec2:DescribeTags"
            ],
            "Resource": [
                "*"
            ]
        }
    ]
}
```

## Step 2 - AWS command line tools

If you already have the AWS command line tools installed in your system, you can go to next step.

Follow the instalation steps in the official [documentation page](https://docs.aws.amazon.com/pt_br/cli/latest/userguide/cli-chap-install.html) or if you prefers you can use the package manager of your system.

**Debian/Ubuntu system based**:

```
$ sudo apt-get install awscli
```

## Step 3 - Configuring AWS command line tools profile

Our script by default uses a profile named `ssbackup`. But you can use another you prefer and inform it during the execution using `--profile` parameter.

Then you need configure the profile with the AWS IAM user credential you created in the first step (you saved?)

Following the [config documentation](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html) you will execute the `aws` command passing `--profile=ssbackup` parameter to the `configure` subcommand.

Answer the questions with the previous saved `AWS Access Key ID` and `AWS Secret Access Key` of your user, the AWS region of your EBS (example: us-east-1) and `json` for default output format.

See the following example:


```
$ aws configure --profile=ssbackup
AWS Access Key ID [None]: your-user-access-key-id
AWS Secret Access Key [None]: your-user-secret-key
Default region name [None]: region-of-your-ebs
Default output format [None]: json
```

## Step 4 - Installation of the script

Clone this projet with GIT:

```
$ GIT clone git@gitlab.com:fval/ebs-snapshots.git
```

## Step 5 - Scheduling

Add the execution of ssbackup.py script into crontab like this:

```
# m   h  dom mon dow   command
  5   0    *   *   *   python3 /path/to/where/you/clone/the/repository/ssbackup.py --volume-ids=vol-a1bcd2e3 --expiry-days=7
```

The script will perform a snapshot of each volume passed to it, then delete any snapshots older than the given number of days.

You can pass more than one volume id at once for `--volume-id` parameter separing it by comma like this:

```
$ python3 /path/to/where/you/clone/the/repository/ssbackup.py --volume-ids=vol-a1bcd2e3,vol-b2cde3f4 --region=us-east-1 --expiry-days=7
```

It will only delete snapshots that it created.

# Parameters

The script receives the following parameters:

-   -i, --volume-ids : EBS volume id to create snapshot backup for
-   -r, --region : AWS region where volumes are located (default: us-east-1)
-   -g, --group-tag : Group tag for snapshots. The script will use this tag to found expired snapshot backups (default: ssbackup)
-   -p, --profile : the AWS-cli profile you use (default: ssbackup)
-   -d, --delete-old : Delete old snapshots? (default: true)
-   -x, --expiry-days : Number of days to keep snapshots (default: 7)

# Execution log

The script will create an ssbackup.log file that sits next to it describing exactly what it did on last run. For example:

```
2019-09-07 01:05:20AM:  Creating snapshot for volume vol-a1bcd2e3...snap-a123b4cd
2019-09-08 01:05:20AM:  Creating snapshot for volume vol-a1bcd2e3...snap-b123c4de
2019-09-08 01:05:22AM:  Deleting snapshot snap-a123b4cd (0 days old)...done
2019-09-09 01:05:20AM:  Creating snapshot for volume vol-a1bcd2e3...snap-c123d4ef
2019-09-09 01:05:22AM:  Deleting snapshot snap-b123c4de (0 days old)...done
```
